#! /usr/bin/python

from xbee.thread import XBee
import serial
import parser
import json

serial_port = serial.Serial('/dev/ttyAMA0', 9600)
xbee = XBee(serial_port)

def pin_logic_low(pin):
	xbee.remote_at(
	    dest_addr=b'\xFF\xFF',
	    command=pin,
	    parameter=b'\x04')

	xbee.remote_at(
	    dest_addr=b'\xFF\xFF',
	    command='WR')
	pass
def pin_logic_high(pin):
	xbee.remote_at(
	    dest_addr=b'\xFF\xFF',
	    command=pin,
	    parameter=b'\x05')

	xbee.remote_at(
	    dest_addr=b'\xFF\xFF',
	    command='WR')
	pass	
def data_sent(data_raw):
	xbee.send('tx', frame_id='0', dest_addr=b'\xFF\xFF', data=data_raw)
	pass
def hex_send(hex_data):
	hex_Data="".join(hex_data.split())
	hex_Data=hex_Data.decode("hex")
	ser.write(hex_Data)
	pass
def read():
	frame_Data=xbee.wait_read_frame()
	return frame_Data
def con_read():
	while True:
	    try:
	        print xbee.wait_read_frame()
	    except KeyboardInterrupt:
	        break
def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
    	print("\nBU BIR JSON")
        print(json.dumps(json.loads(json_thing), indent=indents))
    else:
    	print("\nBU BIR DICT")
        print(json.dumps(json_thing, indent=indents))
    return None


while True:
    try:
    	control="rx"
    	xbee_data=read()
    	if xbee_data['id']==control:
    		pp_json(xbee_data)
    		json_toParse=xbee_data["rf_data"]
    		pp_json(json_toParse)
    		python_obj = json.loads(json_toParse)
    		print ("\n\nADI: "+python_obj["name"])
    		print ("SOYADI: "+python_obj["lastname"])
    		pass
    	else:
    		print("data is not a valid tx data!")

    except KeyboardInterrupt:
        break
