#! /usr/bin/python

from xbee import XBee, ZigBee
import serial

ser = serial.Serial('/dev/ttyAMA0', 9600)

xbee = XBee(ser)
pin="D3"

xbee.remote_at(
    dest_addr=b'\xFF\xFF',
    command=pin,
    parameter=b'\x04')

xbee.remote_at(
    dest_addr=b'\xFF\xFF',
    command='WR')
