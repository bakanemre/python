import smbus
import math
import time

# Guc yonetim register'lari
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
x_ref=0
y_ref=0
def read_byte(adr):

	return bus.read_byte_data(address, adr)
def read_word(adr):
 high = bus.read_byte_data(address, adr)
 low = bus.read_byte_data(address, adr+1)
 val = (high << 8) + low
 return val
def read_word_2c(adr):
 val = read_word(adr)
 if (val >= 0x8000):
 	return -((65535 - val) + 1)
 else:
 	return val
def dist(a,b):

 return math.sqrt((a*a)+(b*b))
def get_y_rotation(x,y,z):
 radians = math.atan2(x, dist(y,z))
 return -math.degrees(radians)
def get_x_rotation(x,y,z):
 radians = math.atan2(y, dist(x,z))
 return math.degrees(radians)
def read_angel():
	x_angel=0
	y_angel=0
	for x in xrange(0,100):
		accel_xout = read_word_2c(0x3b)
		accel_yout = read_word_2c(0x3d)
		accel_zout = read_word_2c(0x3f)
		accel_xout_scaled = accel_xout / 16384.0
		accel_yout_scaled = accel_yout / 16384.0
		accel_zout_scaled = accel_zout / 16384.0
		x_angel+=get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
		y_angel+=get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
		pass
	list=[]
	list.append(x_angel/100)
	list.append(y_angel/100)
	return list
	pass
def reference(read_qty):
	X=0
	Y=0
	for i in range(0,read_qty):
		list=read_angel()
		X+=list[0]
		Y+=list[1]
		print ("IMU ref : %"+str(i*100/read_qty))
		pass
	global y_ref
	global x_ref
	x_ref=X/read_qty
	y_ref=Y/read_qty
	pass

bus = smbus.SMBus(1)
address = 0x68 #MPU6050 I2C adresi

#MPU6050 ilk calistiginda uyku modunda oldugundan, calistirmak icin asagidaki komutu veriyoruz:
bus.write_byte_data(address, power_mgmt_1, 0)

reference(2)
def tilt():
	global y_ref
	global x_ref
	flag=0
	angels=read_angel()
	if angels[0]<x_ref-3 or angels[0]>x_ref+3:
		flag=1
		pass
	if angels[1]<y_ref-3 or angels[1]>y_ref+3:
		flag=1
		pass
	return flag
	pass
def main():
	global y_ref
	global x_ref
	while True:
		flag=0
		angels=read_angel()
		if angels[0]<x_ref-3 or angels[0]>x_ref+3:
			flag=1
			pass
		if angels[1]<y_ref-3 or angels[1]>y_ref+3:
			flag=1
			pass
		print flag
		pass
	pass


if __name__ == '__main__':
	main()