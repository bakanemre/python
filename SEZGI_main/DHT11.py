#!/usr/bin/python

#calismasi icin https://github.com/adafruit/Adafruit_Python_DHT.git library yuklenmesi gerekli
import Adafruit_DHT
sensor=Adafruit_DHT.DHT11
pin=4
# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
def start():
	for x in xrange(1,6):
		humidity_temp, temperature_temp = Adafruit_DHT.read_retry(sensor, pin)
		print ("temp_ref : %"+str(x*20))
		pass
	pass
start()
def read():
	humidity_temp, temperature_temp = Adafruit_DHT.read_retry(sensor, pin)
	if humidity_temp is not None and temperature_temp is not None:
		humidity = humidity_temp
		temperature = temperature_temp
	return temperature,humidity
	pass
def main():
	start()
	while True:
		humidity_temp, temperature_temp = Adafruit_DHT.read_retry(sensor, pin)
		if humidity_temp is not None and temperature_temp is not None:
			humidity = humidity_temp
			temperature = temperature_temp
			print('Temp={0:0.1f}  Humidity={1:0.1f}%'.format(temperature, humidity))
		pass	
	pass

if __name__ == '__main__':
	main()